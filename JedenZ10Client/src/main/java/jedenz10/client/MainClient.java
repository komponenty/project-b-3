package jedenz10.client;

import jedenz10.client.gui.Okno;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainClient {
	
	public void zaladujGUI() {
		
	}
	
	public static void main(String[] args) {
		ConfigurableApplicationContext context = 
	             new ClassPathXmlApplicationContext("Beans.xml");
		
		Okno okno = (Okno) context.getBean("okno");
		okno.showMenu();
		okno.setVisible(true);
		
		context.close();
	}
	
}
