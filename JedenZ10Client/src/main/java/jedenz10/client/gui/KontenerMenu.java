package jedenz10.client.gui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

public class KontenerMenu extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private Okno okno;
	private static final long serialVersionUID = 1L;
	private Image tlo;
	private JButton nowaGra,ranking,zasady,zakoncz;
	private SpringLayout layout;
	
	public KontenerMenu(String plikTla, String plikBgGra, String plikBgRanking, String plikBgZasady, String plikBgZakoncz ) {
		ImageIcon ii = new ImageIcon(plikTla);
        tlo = ii.getImage();
        
        layout = new SpringLayout();
        setLayout(layout);

        ii = new ImageIcon(plikBgGra);
        nowaGra = new JButton(ii);
        nowaGra.setBorderPainted(false);
        nowaGra.setContentAreaFilled(false);
        nowaGra.setFocusPainted(false);
        nowaGra.setOpaque(false);
        layout.putConstraint(SpringLayout.WEST, nowaGra,
				140,
				SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, nowaGra,
				142,
				SpringLayout.NORTH, this);
		nowaGra.addActionListener(this);
        add(nowaGra);
        
        ii = new ImageIcon(plikBgRanking);
        ranking = new JButton(ii);
        ranking.setBorderPainted(false);
        ranking.setContentAreaFilled(false);
        ranking.setFocusPainted(false);
        ranking.setOpaque(false);
        layout.putConstraint(SpringLayout.WEST, ranking,
				140,
				SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, ranking,
				3,
				SpringLayout.SOUTH, nowaGra);
		ranking.addActionListener(this);
        add(ranking);
        
        ii = new ImageIcon(plikBgZasady);
        zasady = new JButton(ii);
        zasady.setBorderPainted(false);
        zasady.setContentAreaFilled(false);
        zasady.setFocusPainted(false);
        zasady.setOpaque(false);
        layout.putConstraint(SpringLayout.WEST, zasady,
				140,
				SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, zasady,
				3,
				SpringLayout.SOUTH, ranking);
		zasady.addActionListener(this);
        add(zasady);
        
        ii = new ImageIcon(plikBgZakoncz);
        zakoncz = new JButton(ii);
        zakoncz.setBorderPainted(false);
        zakoncz.setContentAreaFilled(false);
        zakoncz.setFocusPainted(false);
        zakoncz.setOpaque(false);
        layout.putConstraint(SpringLayout.WEST, zakoncz,
				140,
				SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, zakoncz,
				3,
				SpringLayout.SOUTH, zasady);
		zakoncz.addActionListener(this);
        add(zakoncz);
        
        setDoubleBuffered(true);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
        Graphics2D g2d = (Graphics2D)g;
        g2d.drawImage(tlo, 0, 0, this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == nowaGra) {
			String nick = (String)JOptionPane.showInputDialog(
			                    this,
			                    "Wprowadź nick",
			                    "Nick",
			                    JOptionPane.PLAIN_MESSAGE
			                    );
			if(nick != null) {
				//System.out.println(nick);
				okno.remove(this);
				okno.showGra(nick);
				okno.repaint();
				okno.setVisible(true);
			}
		} else if(e.getSource() == ranking) {
			okno.remove(this);
			okno.showRanking();
			okno.repaint();
			okno.setVisible(true);
		} else if(e.getSource() == zasady) {
			okno.remove(this);
			okno.showZasady();
			okno.repaint();
			okno.setVisible(true);
		} else if(e.getSource() == zakoncz) {
			okno.zamknijPolaczenie();
		}
	}
	
	public void wyswietlKomunikatKoncaGry(String nick) {
		JOptionPane.showMessageDialog(this, "Gra zostala zakonczona wygral gracz: "+nick,"Koniec gry",JOptionPane.INFORMATION_MESSAGE);
	}
	
	public Okno getOkno() {
		return okno;
	}

	public void setOkno(Okno okno) {
		this.okno = okno;
	}
}
