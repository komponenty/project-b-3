package jedenz10.client.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

public class KontenerZasady extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Okno okno;
	private Image tlo;
	private JButton wroc;
	private SpringLayout layout;
	private String zasady;
	
	public KontenerZasady(String plikTla, String plikBgWroc) {
		ImageIcon ii = new ImageIcon(plikTla);
        tlo = ii.getImage();
        
        layout = new SpringLayout();
        setLayout(layout);

        ii = new ImageIcon(plikBgWroc);
        wroc = new JButton(ii);
        wroc.setBorderPainted(false);
        wroc.setContentAreaFilled(false);
        wroc.setFocusPainted(false);
        wroc.setOpaque(false);
        layout.putConstraint(SpringLayout.EAST, wroc,
				5,
				SpringLayout.EAST, this);
		layout.putConstraint(SpringLayout.SOUTH, wroc,
				-50,
				SpringLayout.SOUTH, this);
		wroc.addActionListener(this);
        add(wroc);
        
        setDoubleBuffered(true);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
        Graphics2D g2d = (Graphics2D)g;
        g2d.drawImage(tlo, 0, 0, this);
        g2d.setColor(Color.WHITE);
        g2d.drawString(zasady, 200, 200);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == wroc) {
			okno.remove(this);
			okno.showMenu();
			okno.repaint();
			okno.setVisible(true);
		}
	}

	public Okno getOkno() {
		return okno;
	}

	public void setOkno(Okno okno) {
		this.okno = okno;
	}

	public String getZasady() {
		return zasady;
	}

	public void setZasady(String zasady) {
		this.zasady = zasady;
	}
}
