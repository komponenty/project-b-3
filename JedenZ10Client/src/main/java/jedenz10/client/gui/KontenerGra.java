package jedenz10.client.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SpringLayout;
import javax.swing.Timer;

public class KontenerGra extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Okno okno;
	private Image tlo;
	private JButton start,odpowiedzA,odpowiedzB,odpowiedzC,odpowiedzD;
	private JLabel nickInfo,rundaInfo,odpowiadaInfo,czasInfo,pytanieInfo,historiaInfo;
	private JLabel nickLabel,rundaLabel,odpowiadaLabel,czasLabel,pytanieLabel;
	private JTextArea historia;
	private SpringLayout layout;
	private Timer timer;
	private int licznik=0;
	//private String nick,pytanie;
	
	public KontenerGra(String plikTla, String plikBgStart) {
		ImageIcon ii = new ImageIcon(plikTla);
        tlo = ii.getImage();
        
        layout = new SpringLayout();
        setLayout(layout);
        
        ii = new ImageIcon(plikBgStart);
        start = new JButton(ii);
        start.setBorderPainted(false);
        start.setContentAreaFilled(false);
        start.setFocusPainted(false);
        start.setOpaque(false);
        layout.putConstraint(SpringLayout.WEST, start,
				300,
				SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, start,
				20,
				SpringLayout.NORTH, this);
		start.addActionListener(this);
        add(start);
        
        /*	LABLE-INFO	*/
        
        timer = new Timer(1000, null);
        timer.addActionListener(this);
        
        nickInfo = new JLabel("<html><font color='green'>Gracz:</font></html>");
        rundaInfo = new JLabel("<html><font color='green'>Runda:</font></html>");
        odpowiadaInfo = new JLabel("<html><font color='green'>Na pytanie odpowiada:</font></html>");
        czasInfo = new JLabel("<html><font color='green'>Pozostaly czas:</font></html>");
        pytanieInfo = new JLabel("<html><font color='green'>Pytanie:</font></html>");
        historiaInfo = new JLabel("<html><font color='green'>Historia rozgrywki:</font></html>");
        
        layout.putConstraint(SpringLayout.WEST, nickInfo,
				90,
				SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, nickInfo,
				90,
				SpringLayout.NORTH, this);
        
        layout.putConstraint(SpringLayout.WEST, rundaInfo,
        		90,
				SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, rundaInfo,
				5,
				SpringLayout.SOUTH, nickInfo);
        
        layout.putConstraint(SpringLayout.WEST, odpowiadaInfo,
        		90,
				SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, odpowiadaInfo,
				5,
				SpringLayout.SOUTH, rundaInfo);
        
        layout.putConstraint(SpringLayout.WEST, czasInfo,
        		90,
				SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, czasInfo,
				5,
				SpringLayout.SOUTH, odpowiadaInfo);
        
        layout.putConstraint(SpringLayout.WEST, pytanieInfo,
        		90,
				SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, pytanieInfo,
				5,
				SpringLayout.SOUTH, czasInfo);
        
        layout.putConstraint(SpringLayout.WEST, historiaInfo,
				300,
				SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, historiaInfo,
				90,
				SpringLayout.NORTH, this);
        
        add(nickInfo);
        add(rundaInfo);
        add(odpowiadaInfo);
        add(czasInfo);
        add(pytanieInfo);
        add(historiaInfo);
        
        /*	LABLE-LABEL	*/
        
        nickLabel = new JLabel();
        nickLabel.setForeground(Color.WHITE);
        rundaLabel = new JLabel();
        rundaLabel.setForeground(Color.WHITE);
        odpowiadaLabel = new JLabel();
        odpowiadaLabel.setForeground(Color.WHITE);
        czasLabel = new JLabel();
        czasLabel.setForeground(Color.WHITE);
        pytanieLabel = new JLabel();
        pytanieLabel.setForeground(Color.WHITE);
        
        layout.putConstraint(SpringLayout.WEST, nickLabel,
				5,
				SpringLayout.EAST, nickInfo);
        layout.putConstraint(SpringLayout.NORTH, nickLabel,
				90,
				SpringLayout.NORTH, this);
        
        layout.putConstraint(SpringLayout.WEST, rundaLabel,
        		5,
				SpringLayout.EAST, rundaInfo);
        layout.putConstraint(SpringLayout.NORTH, rundaLabel,
				5,
				SpringLayout.SOUTH, nickInfo);
        
        layout.putConstraint(SpringLayout.WEST, odpowiadaLabel,
        		5,
				SpringLayout.EAST, odpowiadaInfo);
        layout.putConstraint(SpringLayout.NORTH, odpowiadaLabel,
				5,
				SpringLayout.SOUTH, rundaInfo);
        
        layout.putConstraint(SpringLayout.WEST, czasLabel,
        		5,
				SpringLayout.EAST, czasInfo);
        layout.putConstraint(SpringLayout.NORTH, czasLabel,
				5,
				SpringLayout.SOUTH, odpowiadaInfo);
        
        layout.putConstraint(SpringLayout.WEST, pytanieLabel,
        		5,
				SpringLayout.EAST, pytanieInfo);
        layout.putConstraint(SpringLayout.NORTH, pytanieLabel,
				5,
				SpringLayout.SOUTH, czasInfo);
        
        add(nickLabel);
        add(rundaLabel);
        add(odpowiadaLabel);
        add(czasLabel);
        add(pytanieLabel);
        
        /*	MENU OPERACJI	*/
        odpowiedzA = new JButton();
		odpowiedzB = new JButton();
		odpowiedzC = new JButton();
		odpowiedzD = new JButton();
		historia = new JTextArea();
		historia.setEditable(false);
		JScrollPane historiaScroll = new JScrollPane(historia);
		
		odpowiedzA.addActionListener(this);
		odpowiedzB.addActionListener(this);
		odpowiedzC.addActionListener(this);
		odpowiedzD.addActionListener(this);
		
		layout.putConstraint(SpringLayout.WEST, odpowiedzA,
				100,
				SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, odpowiedzA,
				10,
				SpringLayout.SOUTH, pytanieInfo);
		
		layout.putConstraint(SpringLayout.WEST, odpowiedzB,
				5,
				SpringLayout.EAST, odpowiedzA);
		layout.putConstraint(SpringLayout.NORTH, odpowiedzB,
				10,
				SpringLayout.SOUTH, pytanieInfo);
		
		layout.putConstraint(SpringLayout.WEST, odpowiedzC,
				5,
				SpringLayout.EAST, odpowiedzB);
		layout.putConstraint(SpringLayout.NORTH, odpowiedzC,
				10,
				SpringLayout.SOUTH, pytanieInfo);
		
		layout.putConstraint(SpringLayout.WEST, odpowiedzD,
				5,
				SpringLayout.EAST, odpowiedzC);
		layout.putConstraint(SpringLayout.NORTH, odpowiedzD,
				10,
				SpringLayout.SOUTH, pytanieInfo);
		
		layout.putConstraint(SpringLayout.WEST, historiaScroll,
				300,
				SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, historiaScroll,
				5,
				SpringLayout.SOUTH, historiaInfo);
		layout.putConstraint(SpringLayout.SOUTH, historiaScroll,
				-30,
				SpringLayout.NORTH, odpowiedzD);
		layout.putConstraint(SpringLayout.EAST, historiaScroll,
				-10,
				SpringLayout.EAST, this);
		
		add(odpowiedzA);
		add(odpowiedzB);
		add(odpowiedzC);
		add(odpowiedzD);
		add(historiaScroll);
        
        setDoubleBuffered(true);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
        Graphics2D g2d = (Graphics2D)g;
        g2d.drawImage(tlo, 0, 0, this);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == start) {
			okno.wyslijWiadomoscStart();
		} else if(e.getSource() == odpowiedzA) {
			okno.wyslijOdpowiedz(pytanieLabel.getText(), nickLabel.getText(), odpowiedzA.getText());
		} else if(e.getSource() == odpowiedzB) {
			okno.wyslijOdpowiedz(pytanieLabel.getText(), nickLabel.getText(), odpowiedzB.getText());
		} else if(e.getSource() == odpowiedzC) {
			okno.wyslijOdpowiedz(pytanieLabel.getText(), nickLabel.getText(), odpowiedzC.getText());
		} else if(e.getSource() == odpowiedzD) {
			okno.wyslijOdpowiedz(pytanieLabel.getText(), nickLabel.getText(), odpowiedzD.getText());
		} else if(e.getSource() == timer) {
			if(licznik>=0) {
				czasLabel.setText(""+(licznik--));
			} else {
				timer.stop();
				okno.wyslijOdpowiedz("brakOdpowiedzi", nickLabel.getText(), odpowiedzD.getText());
			}
			
		}
	}
	
	/*public void wyswietlRunde(String runda, String odpowiadajacy, String pytanie, String[] podpowiedzi) {
		//super.paintComponent(getGraphics());
		Graphics g = (Graphics) getGraphics();
        g.setColor(Color.WHITE);
		g.drawString("Runda: "+runda, 100, 120);
		g.drawString("Na pytanie odpowiada: "+odpowiadajacy, 100, 140);
		g.drawString("Pozostaly Czas", 100, 160);
		g.drawString("Pytanie: "+pytanie, 100, 180);
		g.drawString("Historia Rozgrywki", 300, 100);
		wyswietlPodpowiedzi(podpowiedzi);
		this.pytanie = pytanie;
		start.setVisible(false);
	}*/
	
	/*public void wyswietlPodpowiedzi(String[] podpowiedzi) {
		odpowiedzA = new JButton(podpowiedzi[0]);
		odpowiedzB = new JButton(podpowiedzi[1]);
		odpowiedzC = new JButton(podpowiedzi[2]);
		odpowiedzD = new JButton(podpowiedzi[3]);
		historia = new JTextArea();
		historia.setEditable(false);
		JScrollPane historiaScroll = new JScrollPane(historia);
		
		odpowiedzA.addActionListener(this);
		odpowiedzB.addActionListener(this);
		odpowiedzC.addActionListener(this);
		odpowiedzD.addActionListener(this);
		
		layout.putConstraint(SpringLayout.WEST, odpowiedzA,
				100,
				SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, odpowiedzA,
				190,
				SpringLayout.NORTH, this);
		
		layout.putConstraint(SpringLayout.WEST, odpowiedzB,
				5,
				SpringLayout.EAST, odpowiedzA);
		layout.putConstraint(SpringLayout.NORTH, odpowiedzB,
				190,
				SpringLayout.NORTH, this);
		
		layout.putConstraint(SpringLayout.WEST, odpowiedzC,
				5,
				SpringLayout.EAST, odpowiedzB);
		layout.putConstraint(SpringLayout.NORTH, odpowiedzC,
				190,
				SpringLayout.NORTH, this);
		
		layout.putConstraint(SpringLayout.WEST, odpowiedzD,
				5,
				SpringLayout.EAST, odpowiedzC);
		layout.putConstraint(SpringLayout.NORTH, odpowiedzD,
				190,
				SpringLayout.NORTH, this);
		
		layout.putConstraint(SpringLayout.WEST, historiaScroll,
				300,
				SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, historiaScroll,
				105,
				SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.SOUTH, historiaScroll,
				-30,
				SpringLayout.NORTH, odpowiedzD);
		layout.putConstraint(SpringLayout.EAST, historiaScroll,
				-10,
				SpringLayout.EAST, this);
		
		add(odpowiedzA);
		add(odpowiedzB);
		add(odpowiedzC);
		add(odpowiedzD);
		add(historiaScroll);
	}*/
	
	public Okno getOkno() {
		return okno;
	}

	public void setOkno(Okno okno) {
		this.okno = okno;
	}

	public JButton getStart() {
		return start;
	}

	public void setStart(JButton start) {
		this.start = start;
	}

	public JButton getOdpowiedzA() {
		return odpowiedzA;
	}

	public void setOdpowiedzA(JButton odpowiedzA) {
		this.odpowiedzA = odpowiedzA;
	}

	public JButton getOdpowiedzB() {
		return odpowiedzB;
	}

	public void setOdpowiedzB(JButton odpowiedzB) {
		this.odpowiedzB = odpowiedzB;
	}

	public JButton getOdpowiedzC() {
		return odpowiedzC;
	}

	public void setOdpowiedzC(JButton odpowiedzC) {
		this.odpowiedzC = odpowiedzC;
	}

	public JButton getOdpowiedzD() {
		return odpowiedzD;
	}

	public void setOdpowiedzD(JButton odpowiedzD) {
		this.odpowiedzD = odpowiedzD;
	}

	public JTextArea getHistoria() {
		return historia;
	}

	public void setHistoria(JTextArea historia) {
		this.historia = historia;
	}

	public JLabel getNickInfo() {
		return nickInfo;
	}

	public void setNickInfo(JLabel nickInfo) {
		this.nickInfo = nickInfo;
	}

	public JLabel getRundaInfo() {
		return rundaInfo;
	}

	public void setRundaInfo(JLabel rundaInfo) {
		this.rundaInfo = rundaInfo;
	}

	public JLabel getOdpowiadaInfo() {
		return odpowiadaInfo;
	}

	public void setOdpowiadaInfo(JLabel odpowiadaInfo) {
		this.odpowiadaInfo = odpowiadaInfo;
	}

	public JLabel getCzasInfo() {
		return czasInfo;
	}

	public void setCzasInfo(JLabel czasInfo) {
		this.czasInfo = czasInfo;
	}

	public JLabel getPytanieInfo() {
		return pytanieInfo;
	}

	public void setPytanieInfo(JLabel pytanieInfo) {
		this.pytanieInfo = pytanieInfo;
	}

	public JLabel getHistoriaInfo() {
		return historiaInfo;
	}

	public void setHistoriaInfo(JLabel historiaInfo) {
		this.historiaInfo = historiaInfo;
	}

	public JLabel getNickLabel() {
		return nickLabel;
	}

	public void setNickLabel(JLabel nickLabel) {
		this.nickLabel = nickLabel;
	}

	public JLabel getRundaLabel() {
		return rundaLabel;
	}

	public void setRundaLabel(JLabel rundaLabel) {
		this.rundaLabel = rundaLabel;
	}

	public JLabel getOdpowiadaLabel() {
		return odpowiadaLabel;
	}

	public void setOdpowiadaLabel(JLabel odpowiadaLabel) {
		this.odpowiadaLabel = odpowiadaLabel;
	}

	public JLabel getCzasLabel() {
		return czasLabel;
	}

	public void setCzasLabel(JLabel czasLabel) {
		this.czasLabel = czasLabel;
	}

	public JLabel getPytanieLabel() {
		return pytanieLabel;
	}

	public void setPytanieLabel(JLabel pytanieLabel) {
		this.pytanieLabel = pytanieLabel;
	}

	public Timer getTimer() {
		return timer;
	}

	public void setTimer(Timer timer) {
		this.timer = timer;
	}

	public int getLicznik() {
		return licznik;
	}

	public void setLicznik(int licznik) {
		this.licznik = licznik;
	}
	
	
}
