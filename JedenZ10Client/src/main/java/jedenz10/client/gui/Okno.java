/**
 * 
 */
package jedenz10.client.gui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.JFrame;

import jedenz10.client.Nasluchiwacz;

public class Okno extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String tytul = "Jeden z dziesieciu v:";
	private KontenerGra gra;
	private KontenerMenu menu;
	private KontenerZasady zasady;
	private KontenerRanking ranking;
	
	private static final int PORT = 4444;
	private static final String HOST = "localhost";
	private Socket socket;
	private PrintWriter out;
	private Thread nasluchiwacz;
	
	public Okno(int szerokosc, int wysokosc, boolean isResizable, double wersja) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    setSize(szerokosc, wysokosc);
	    //setLocationRelativeTo(null);
	    setTitle(tytul+" "+wersja);
	    setResizable(isResizable);
	    addWindowListener( new WindowAdapter() {
	        public void windowClosing(WindowEvent e)
	        { zamknijPolaczenie(); }
	      });
	    nawiazPolaczenie();
	    //wyslijWiadomoscTestowa();
	}
	
	private void nawiazPolaczenie() {
		try {
		      socket = new Socket(HOST, PORT);
		      BufferedReader in  = new BufferedReader( 
				  		new InputStreamReader( socket.getInputStream() ) );
		      out = new PrintWriter( socket.getOutputStream(), true );  // autoflush

		     nasluchiwacz = new Nasluchiwacz(this,in);    // start watching for server msgs
		     nasluchiwacz.start();
		    }
		    catch(Exception e)
		    {  System.out.println(e);  }
	}
	
	protected void zamknijPolaczenie() {
		try {
		      out.println("koniec");    // tell server that client is disconnecting
		      //socket.close();
		    }
		    catch(Exception e)
		    {  System.out.println( e );  }

		    System.exit( 0 );
	}
	
	protected void wyslijWiadomoscTestowa() {
		//String wiadomosc = "ranking";
		out.println("ranking");
	}
	
	public void wyslijWiadomoscStart() {
		out.println("start");
	}
	
	public void wyslijOdpowiedz(String pytanie, String nick, String odpowiedz) {
		out.println("odpowiedz="+pytanie+";"+nick+";"+odpowiedz);
	}
	
	public void showGra(String nick) {
		//removeAll();
		out.println("nick="+nick);
		add(gra);
		gra.getNickLabel().setText(nick);
	}
	
	public void showMenu() {
		//removeAll();
		add(menu);
	}
	
	public void showZasady() {
		//removeAll();
		out.println("zasady");
		add(zasady);
		
	}
	
	public void showRanking() {
		out.println("ranking");
		add(ranking);
	}

	public String getTytul() {
		return tytul;
	}

	public void setTytul(String tytul) {
		this.tytul = tytul;
	}

	public KontenerGra getGra() {
		return gra;
	}

	public void setGra(KontenerGra gra) {
		this.gra = gra;
	}

	public KontenerMenu getMenu() {
		return menu;
	}

	public void setMenu(KontenerMenu menu) {
		this.menu = menu;
	}

	public KontenerZasady getZasady() {
		return zasady;
	}

	public void setZasady(KontenerZasady zasady) {
		this.zasady = zasady;
	}

	public KontenerRanking getRanking() {
		return ranking;
	}

	public void setRanking(KontenerRanking ranking) {
		this.ranking = ranking;
	}
	
}
