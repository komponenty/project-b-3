package jedenz10.client.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import jedenz10.client.Gracz;

public class KontenerRanking extends JPanel implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Okno okno;
	private Image tlo;
	private JButton wroc;
	private SpringLayout layout;
	private ArrayList<Gracz> gracze = new ArrayList<Gracz>();
	
	public KontenerRanking(String plikTla, String plikBgWroc) {
		ImageIcon ii = new ImageIcon(plikTla);
        tlo = ii.getImage();
        
        layout = new SpringLayout();
        setLayout(layout);

        ii = new ImageIcon(plikBgWroc);
        wroc = new JButton(ii);
        wroc.setBorderPainted(false);
        wroc.setContentAreaFilled(false);
        wroc.setFocusPainted(false);
        wroc.setOpaque(false);
        layout.putConstraint(SpringLayout.EAST, wroc,
				5,
				SpringLayout.EAST, this);
		layout.putConstraint(SpringLayout.SOUTH, wroc,
				-50,
				SpringLayout.SOUTH, this);
		wroc.addActionListener(this);
        add(wroc);
        setDoubleBuffered(true);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
        Graphics2D g2d = (Graphics2D)g;
        g2d.drawImage(tlo, 0, 0, this);
        
        int y=150;
        g2d.setColor(Color.WHITE);
        for(int i=0; i < gracze.size(); i++,y+=30) {
        	g2d.drawString(""+gracze.get(i).getMiejsce(), 160, y);
        	g2d.drawString(gracze.get(i).nickaGracza(), 248, y);
        	g2d.drawString(""+gracze.get(i).liczbaPunktow(), 345, y);
        }
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == wroc) {
			okno.remove(this);
			okno.showMenu();
			okno.repaint();
			okno.setVisible(true);
		}
	}

	public Okno getOkno() {
		return okno;
	}

	public void setOkno(Okno okno) {
		this.okno = okno;
	}

	public ArrayList<Gracz> getGracze() {
		return gracze;
	}

	public void setGracze(ArrayList<Gracz> gracze) {
		this.gracze = gracze;
	}
}
