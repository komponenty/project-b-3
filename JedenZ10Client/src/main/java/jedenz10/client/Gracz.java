package jedenz10.client;

import jedenz10.client.interfejsy.IGracz;

public class Gracz implements IGracz,Comparable<Gracz> {
	
	private int miejsce;
	private String nickGracza;
	private Integer liczbaPunktow;
	
	public Gracz(int miejsce, String nickGracza, int liczbaPunktow) {
		this.miejsce = miejsce;
		this.nickGracza = nickGracza;
		this.liczbaPunktow = liczbaPunktow;
	}
	
	public Gracz(String nickGracza, int liczbaPunktow) {
		this.nickGracza = nickGracza;
		this.liczbaPunktow = liczbaPunktow;
	}
	
	
	
	public int getMiejsce() {
		return miejsce;
	}

	public void setMiejsce(int miejsce) {
		this.miejsce = miejsce;
	}

	@Override
	public String nickaGracza() {
		return nickGracza;
	}

	@Override
	public int liczbaPunktow() {
		return liczbaPunktow;
	}
	
	public void liczbaPunktow(int liczbaPunktow) {
		this.liczbaPunktow += liczbaPunktow;
	}
	
	@Override
	public String toString() {
		return nickGracza+";"+liczbaPunktow;
	}

	@Override
	public int compareTo(Gracz o) {
		// TODO Auto-generated method stub
		return o.liczbaPunktow.compareTo(liczbaPunktow);
	}

}
