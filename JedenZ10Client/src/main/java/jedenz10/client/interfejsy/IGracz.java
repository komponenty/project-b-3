/**
 * 
 */
package jedenz10.client.interfejsy;

public interface IGracz {
	String nickaGracza();
	int liczbaPunktow();
}
