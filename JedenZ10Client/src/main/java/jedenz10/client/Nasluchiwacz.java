package jedenz10.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import jedenz10.client.gui.Okno;

public class Nasluchiwacz extends Thread {
	
	private BufferedReader in;
	private Okno okno;
	
	public Nasluchiwacz(Okno okno, BufferedReader in) {
		this.okno = okno;
		this.in = in;
	}
	
	public void run() {
		String linia;
		try {
			while((linia = in.readLine()) != null) {
				wykonajRzadanie(linia);
			}
		} catch(Exception e) {
			//e.printStackTrace();
			System.err.println("connection lost");
			System.exit(0);
		}
	}
	
	private void wykonajRzadanie(String linia) throws IOException {
		if(linia.matches("^ranking=.*$")) {
			String linia1 = linia.replaceFirst("^ranking=", "");
			//System.out.println(linia1);
			StringTokenizer st = new StringTokenizer(linia1,"&");
			int i = 0;
			ArrayList<Gracz> ranking = new ArrayList<Gracz>();
			while(st.hasMoreTokens()) {
				String wiersz = st.nextToken();
				String[] rankingWiersz = wiersz.split(";");
				ranking.add(new Gracz(++i,rankingWiersz[0],Integer.parseInt(rankingWiersz[1])));
			}
			okno.getRanking().setGracze(ranking);
			//for(int j=0; j < ranking.size(); j++) {
			//	System.out.println(ranking.get(j));
			//}
		} else if(linia.matches("^zasady=.*$")) {
			String linia1 = linia.replaceFirst("^zasady=", "");
			okno.getZasady().setZasady(linia1);
		} else if(linia.matches("^host$")) {
			okno.getGra().getStart().setEnabled(true);
		} else if(linia.matches("^nohost$")) {
			okno.getGra().getStart().setEnabled(false);
		} else if(linia.matches("^runda1;.*$")) {
			String[] wiadomosc = linia.split(";");
			//String[] podpowiedzi = {wiadomosc[4],wiadomosc[5],wiadomosc[6],wiadomosc[7]};
			okno.getGra().getRundaLabel().setText(wiadomosc[0]);
			okno.getGra().getOdpowiadaLabel().setText(wiadomosc[2].replaceFirst("^nick=", ""));
			okno.getGra().getPytanieLabel().setText(wiadomosc[3]);
			okno.getGra().getOdpowiedzA().setText(wiadomosc[4]);
			okno.getGra().getOdpowiedzB().setText(wiadomosc[5]);
			okno.getGra().getOdpowiedzC().setText(wiadomosc[6]);
			okno.getGra().getOdpowiedzD().setText(wiadomosc[7]);
			okno.getGra().getStart().setEnabled(false);
			okno.getGra().getOdpowiedzA().setEnabled(true);
			okno.getGra().getOdpowiedzB().setEnabled(true);
			okno.getGra().getOdpowiedzC().setEnabled(true);
			okno.getGra().getOdpowiedzD().setEnabled(true);
			okno.getGra().setLicznik(Integer.parseInt(wiadomosc[1]));
			okno.getGra().getTimer().start();
			//okno.getGra().wyswietlRunde(wiadomosc[0],wiadomosc[2].replaceFirst("^nick=", ""),wiadomosc[3],podpowiedzi);
			if(!okno.getGra().getNickLabel().getText().equals(wiadomosc[2].replaceFirst("^nick=", ""))) {
				okno.getGra().getOdpowiedzA().setEnabled(false);
				okno.getGra().getOdpowiedzB().setEnabled(false);
				okno.getGra().getOdpowiedzC().setEnabled(false);
				okno.getGra().getOdpowiedzD().setEnabled(false);
			}
		} else if(linia.matches("^historia=.*$")) {
			String[] wartosci = linia.replaceFirst("^historia=", "").split(";");
			//okno.getGra().getHistoria().append("abc");
			okno.getGra().getHistoria().append("Gracz: "+wartosci[0].replaceFirst("^nick=", "")+" "+"Punkty: "+wartosci[1]+"\n");
		} else if(linia.matches("^zwyciezca=.*$")) {
			okno.getGra().getTimer().stop();
			okno.remove(okno.getGra());
			okno.showMenu();
			okno.repaint();
			okno.setVisible(true);
			String[] tekst = linia.split("=");
			okno.getMenu().wyswietlKomunikatKoncaGry(tekst[2]);
		}
	}
}
