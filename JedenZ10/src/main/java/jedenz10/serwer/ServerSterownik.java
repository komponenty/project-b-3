package jedenz10.serwer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ServerSterownik extends Thread {
	
	private Socket socketKlienta;
	private String adresKlienta;
	private int port;
	private GraczNet gracz;
	
	private SterownikGry sg;
	
	public ServerSterownik(Socket s, SterownikGry sg) {
		this.sg = sg;
		socketKlienta = s;
		adresKlienta = socketKlienta.getInetAddress().getHostAddress();
		port = socketKlienta.getPort();
		System.out.println("Client connection from (" + 
				adresKlienta + ", " + port + ")");
	}
	
	public void run() {
		try {
			BufferedReader in = new BufferedReader(
					new InputStreamReader( socketKlienta.getInputStream() ));
			PrintWriter out =
					new PrintWriter( socketKlienta.getOutputStream(), true);
			
			sg.dodajGracza(adresKlienta, port, out);
			
			
			zarzadzajKlientem(in, out);
			
			sg.usunGracza(adresKlienta, port);
			if(gracz != null) {
				sg.usunGraczaZGry(gracz);
			}
			socketKlienta.close();
			
			System.out.println("Client (" + adresKlienta + ", " + 
					port + ") connection closed\n");
		} catch(Exception e) {
			System.err.println(e);
		}
		
	}
	
	private void zarzadzajKlientem(BufferedReader in, PrintWriter out) {
		String linia;
		boolean koniec = false;
		try {
			while(!koniec) {
				if((linia = in.readLine()) == null) {
					koniec = true;
				} else {
					System.out.println("[FROM]Client (" + adresKlienta + ", " + 
							port + ") [MESSAGE]: " + linia);
					if(linia.trim().equals("koniec")) {
						koniec = true;
					} else {
						wykonajRzadanie(linia, out);
					}
				}
			}
		} catch(IOException e) {
			System.err.println("error");
			System.err.println(e);
		}
	}
	
	private void wykonajRzadanie(String linia, PrintWriter out) {
		switch(linia) {
		case "ranking": {
			out.println("ranking="+sg.ranking());
		};
		break;
		case "zasady": {
			out.println("zasady="+sg.zasady());
		}
		break;
		}
		
		if(linia.matches("^nick=.*$")) {
			gracz = new GraczNet(linia.replace("^nick=", ""),0,adresKlienta, port, out);
			sg.DodajGraczaDoGry(gracz);
			if(sg.jestHostem(gracz)) {
				out.println("host");
			} else {
				out.println("nohost");
			}
		} else if(linia.matches("^start$")) {
			System.out.println(gracz.nickaGracza());
			sg.zacznijGre(gracz.nickaGracza());
		} else if(linia.matches("^odpowiedz=.*$")) {
			String[] elementy = linia.replaceFirst("^odpowiedz=", "").split(";");
			//System.out.println(elementy[0]);
			//System.out.println(elementy[2]);
			for(int i=0; i < sg.getBk().getListaKategorii().size();i++) {
				for(int j=0; j < sg.getBk().getListaKategorii().get(i).getListaPytan().size() ;j++) {
					if(sg.getBk().getListaKategorii().get(i).getListaPytan().get(j).getPytanie().equals(elementy[0])) {
						if(sg.getBk().getListaKategorii().get(i).getListaPytan().get(j).czyPrawidlowa(elementy[2])) {
							sg.dodajPunkty(elementy[1], 5);
						} else {
							sg.dodajPunkty(elementy[1], 0);
						}
					}
				}
			}
			sg.zadajPytanie(gracz);
		}
	}
	
}
