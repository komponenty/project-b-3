package jedenz10.serwer;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import jedenz10.serwer.bazapytan.BazaKategorii;
import jedenz10.serwer.interfejsy.INetGra;
import jedenz10.serwer.interfejsy.INetRanking;
import jedenz10.serwer.interfejsy.INetZasady;
import jedenz10.serwer.ranking.ListaRankingowa;

public class SterownikGry implements INetRanking,INetZasady,INetGra {
	private String zasady;
	private ArrayList<Gracz> gracze;
	private ArrayList<Gracz> graczeWRundzie;
	private ListaRankingowa lr;
	private BazaKategorii bk;
	
	public SterownikGry(ListaRankingowa lr, BazaKategorii bk) {
		this.lr = lr;
		this.bk = bk;
		gracze = new ArrayList<Gracz>();
		graczeWRundzie = new ArrayList<Gracz>();
	}
	
	@Override
	synchronized public String ranking() {
		Object[] ranking = lr.ranking().toArray();
		Arrays.sort(ranking);
		String pakietRanking = ranking[0].toString();
		for(int i = 1; i < ranking.length; i++) {
			pakietRanking += "&"+ranking[i].toString();
		}
		return pakietRanking;
	}

	@Override
	synchronized public String zasady() {
		return zasady;
	}
	
	synchronized public void dodajGracza(String adresKlienta, int port, PrintWriter out) {
		gracze.add(new GraczNet("", 0, adresKlienta, port, out));
	}
	
	synchronized public void usunGracza(String adresKlienta, int port) {
		GraczNet g;
		for(int i=0; i < gracze.size(); i++) {
			g = (GraczNet) gracze.get(i);
			if(g.sprawdzPoprawnosc(adresKlienta, port)) {
				gracze.remove(i);
			}
		}
	}
	
	@Override
	synchronized public void DodajGraczaDoGry(GraczNet gracz) {
		graczeWRundzie.add(gracz);
	}
	
	synchronized public void usunGraczaZGry(Gracz gracz) {
		if(graczeWRundzie.contains(gracz)) {
			graczeWRundzie.remove(gracz);
		}
	}
	
	synchronized public boolean jestHostem(Gracz gracz) {
		//System.out.println(graczeWRundzie.size());
		if(graczeWRundzie.get(0).equals(gracz)) {
			return true;
		}
		return false;
	}

	@Override
	synchronized public void zadajPytanie(GraczNet gracz) {
		Pytanie p = bk.losujPytanie("latwe");
		GraczNet gn;
		String[] podpowiedzi = p.podPodpowiedzi();
		String podpowiedziString=podpowiedzi[0];
		for(int j=1; j < podpowiedzi.length; j++) {
			podpowiedziString+=";"+podpowiedzi[j];
		}
		int indeks = graczeWRundzie.indexOf(gracz);
		//System.out.println(indeks);
		if(indeks == graczeWRundzie.size()-1) {
			for(int i=0; i < graczeWRundzie.size(); i++) {
				gn = (GraczNet) graczeWRundzie.get(i);
				gn.getOut().println("runda1"+";"+"10"+";"+graczeWRundzie.get(0).nickaGracza()+";"+p.getPytanie()+";"+podpowiedziString);
			}
		} else {
			for(int i=0; i < graczeWRundzie.size(); i++) {
				gn = (GraczNet) graczeWRundzie.get(i);
				gn.getOut().println("runda1"+";"+"10"+";"+graczeWRundzie.get(indeks+1).nickaGracza()+";"+p.getPytanie()+";"+podpowiedziString);
			}
		}
	}

	@Override
	synchronized public void zacznijGre(String nick) {
		Pytanie p = bk.losujPytanie("latwe");
		GraczNet gn;
		String[] podpowiedzi = p.podPodpowiedzi();
		String podpowiedziString=podpowiedzi[0];
		for(int j=1; j < podpowiedzi.length; j++) {
			podpowiedziString+=";"+podpowiedzi[j];
		}
		for(int i=0; i < graczeWRundzie.size(); i++) {
			gn = (GraczNet) graczeWRundzie.get(i);
			gn.getOut().println("runda1"+";"+"10"+";"+nick+";"+p.getPytanie()+";"+podpowiedziString);
		}
		Timer timer = new Timer();
		//czas rundy 2min
		timer.schedule(new ZegarRundy(), 2*60*1000);
	}
	
	private class ZegarRundy extends TimerTask {

		@Override
		public void run() {
			GraczNet gn;
			String zwyciezca = ZapiszRanking();
			for(int i=0; i < graczeWRundzie.size(); i++) {
				gn = (GraczNet) graczeWRundzie.get(i);
				gn.getOut().println("zwyciezca="+zwyciezca);
			}
		}
		
	}
	
	@Override
	synchronized public void dodajPunkty(String nick, int ilosc) {
		GraczNet gn;
		int iloscSend=0;
		for(int i=0; i < graczeWRundzie.size(); i++) {
			if(graczeWRundzie.get(i).nickaGracza().replaceFirst("^nick=","").equals(nick)) {
				graczeWRundzie.get(i).liczbaPunktow(ilosc);
				iloscSend = graczeWRundzie.get(i).liczbaPunktow();
			}
		}
		for(int i=0; i < graczeWRundzie.size(); i++) {
			gn = (GraczNet) graczeWRundzie.get(i);
			gn.getOut().println("historia="+nick+";"+iloscSend);
		}
	}
	
	@Override
	synchronized public String ZapiszRanking() {
		Gracz[] gr = new Gracz[graczeWRundzie.size()];
		Gracz[] grwr = graczeWRundzie.toArray(gr);
		Arrays.sort(grwr);
		lr.dodajPunkty(grwr[0].nickaGracza().replaceFirst("^nick=",""), grwr[0].liczbaPunktow());
		return grwr[0].nickaGracza();
	}

	synchronized public String getZasady() {
		return zasady;
	}

	synchronized public void setZasady(String zasady) {
		this.zasady = zasady;
	}

	synchronized public ArrayList<Gracz> getGraczeWRundzie() {
		return graczeWRundzie;
	}

	synchronized public void setGraczeWRundzie(ArrayList<Gracz> graczeWRundzie) {
		this.graczeWRundzie = graczeWRundzie;
	}

	synchronized public BazaKategorii getBk() {
		return bk;
	}

	synchronized public void setBk(BazaKategorii bk) {
		this.bk = bk;
	}
	
	
	
}
