package jedenz10.serwer;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class SterownikGryAspect {

	@Pointcut("execution(* jedenz10.serwer.ranking.ListaRankingowa.ranking())")
	private void selectListaRankingowa() {}
	
	@Before("selectListaRankingowa()")
	public void przedPobraniemListy() {
		System.out.println("[LOAD] Lista rankingowa");
	}
	
	@AfterReturning("selectListaRankingowa()")
	public void poPobraniuListy() {
		System.out.println("[OK] Lista rankingowa");
	}
	
	@AfterThrowing("selectListaRankingowa()")
	public void poBlednymPobraniuListy() {
		System.out.println("[ERROR] Ranking");
	}
	
	@Pointcut("execution(* jedenz10.serwer.SterownikGry.ranking())")
	private void selectRanking() {}
	
	@Before("selectRanking()")
	public void beforeAdvice() {
		System.out.println("[FUNCTION-LOAD]Ranking");
	}
	
	@AfterReturning("selectRanking()")
	public void afterAdvice() {
		System.out.println("[FUNCTION-OK]Ranking");
	}
	
}
