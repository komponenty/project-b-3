package jedenz10.serwer;

import java.io.PrintWriter;

public class GraczNet extends Gracz {
	
	private String adresKlienta;
	private int port;
	private PrintWriter out;
	
	public GraczNet(String nickGracza, int liczbaPunktow, String adresKlienta, int port, PrintWriter out) {
		super(nickGracza,liczbaPunktow);
		this.adresKlienta = adresKlienta;
		this.port = port;
		this.out = out;
	}
	
	public boolean sprawdzPoprawnosc(String adresKlienta, int port) {
		if (this.adresKlienta.equals(adresKlienta) && (this.port == port)) return true;
		return false;
	}
	
	public void wyslijWiadomosc(String wiadomosc) {
		out.println(wiadomosc);
	}
	
	@Override
	public String toString() {
		return adresKlienta + ";" + port + ";";
	}
	
	public PrintWriter getOut() {
		return out;
	}

	public void setOut(PrintWriter out) {
		this.out = out;
	}
	
	
}
