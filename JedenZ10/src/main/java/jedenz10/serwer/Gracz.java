package jedenz10.serwer;

import jedenz10.serwer.interfejsy.IGracz;

public class Gracz implements IGracz,Comparable<Gracz> {
	
	private String nickGracza;
	private Integer liczbaPunktow;
	
	public Gracz(String nickGracza, int liczbaPunktow) {
		this.nickGracza = nickGracza;
		this.liczbaPunktow = liczbaPunktow;
	}
	
	@Override
	public String nickaGracza() {
		return nickGracza;
	}

	@Override
	public int liczbaPunktow() {
		return liczbaPunktow;
	}
	
	public void liczbaPunktow(int liczbaPunktow) {
		this.liczbaPunktow += liczbaPunktow;
	}
	
	@Override
	public String toString() {
		return nickGracza+";"+liczbaPunktow;
	}
	
	@Override
	public int compareTo(Gracz o) {
		// TODO Auto-generated method stub
		return o.liczbaPunktow.compareTo(liczbaPunktow);
	}

}
