package jedenz10.serwer;

import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	
	private int port;
	private SterownikGry sg;
	ServerSocket socketServera;
	
	public Server(SterownikGry sg, int port) {
		this.sg = sg;
		this.port = port;
	}
	
	protected void start() {
		try {
			  socketServera = new ServerSocket(port);
		      Socket socketKlienta;

		      while (true) {
		        System.out.println("Oczekiwanie na klienta...");
		        socketKlienta = socketServera.accept();
		        new ServerSterownik(socketKlienta, sg).start();
		      }
		    }
		    catch(Exception e)
		    {  System.out.println(e);  }
	}
	
	protected void stop() {
		try {
			socketServera.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.err.println(e);
		}
	}
	
	public int getPort() {
		return port;
	}	
}
