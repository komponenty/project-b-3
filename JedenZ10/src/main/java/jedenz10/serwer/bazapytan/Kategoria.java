package jedenz10.serwer.bazapytan;

import java.util.ArrayList;

import jedenz10.serwer.Pytanie;

public class Kategoria {
	
	private ArrayList<Pytanie> listaPytan;
	private String nazwa;
	
	public Kategoria(String nazwa) {
		this.nazwa = nazwa;
		listaPytan = new ArrayList<Pytanie>();
	}
	
	public Kategoria(ArrayList<Pytanie> listaPytan, String nazwa) {
		this.listaPytan = listaPytan;
		this.nazwa = nazwa;
	}

	public ArrayList<Pytanie> getListaPytan() {
		return listaPytan;
	}

	public void setListaPytan(ArrayList<Pytanie> listaPytan) {
		this.listaPytan = listaPytan;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	@Override
	public String toString() {
		return "Kategoria [listaPytan=" + listaPytan + ", nazwa=" + nazwa + "]";
	}
	
	
}
