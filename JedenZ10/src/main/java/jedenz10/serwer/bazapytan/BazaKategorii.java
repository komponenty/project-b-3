package jedenz10.serwer.bazapytan;

import java.util.ArrayList;
import java.util.Random;

import jedenz10.serwer.Pytanie;
import jedenz10.serwer.interfejsy.IBazaPytan;

public class BazaKategorii implements IBazaPytan {
	
	private ArrayList<Kategoria> listaKategorii;
	
	public BazaKategorii() {
		listaKategorii = new ArrayList<Kategoria>();
	}
	
	@Override
	public Pytanie losujPytanie() {
		Random r = new Random();
		int lkR = r.nextInt(listaKategorii.size());
		int pytanieR = r.nextInt(listaKategorii.get(lkR).getListaPytan().size());
		return listaKategorii.get(lkR).getListaPytan().get(pytanieR);
	}

	@Override
	public Pytanie losujPytanie(String kategoria) {
		for(int i=0; i < listaKategorii.size(); i++) {
			if(listaKategorii.get(i).getNazwa().equals(kategoria)) {
				Random r = new Random();
				int pytanieR = r.nextInt(listaKategorii.get(i).getListaPytan().size());
				return listaKategorii.get(i).getListaPytan().get(pytanieR);
			}
		}
		return null;
	}
	
	public void dodajPytanie(Pytanie pytanie) {
		if(pytanie.kategoria().equals("latwe")) {
			listaKategorii.get(0).getListaPytan().add(pytanie);
		} else if(pytanie.kategoria().equals("srednie")) {
			listaKategorii.get(1).getListaPytan().add(pytanie);
		} else if(pytanie.kategoria().equals("trudne")) {
			listaKategorii.get(2).getListaPytan().add(pytanie);
		}
		
	}

	public void usunPytanie(Pytanie pytanie) {
		for(int i=0; i < listaKategorii.size(); i++) {
			if(listaKategorii.get(i).getListaPytan().contains(pytanie)) {
				listaKategorii.get(i).getListaPytan().remove(pytanie);
			}
		}
	}

	public ArrayList<Kategoria> getListaKategorii() {
		return listaKategorii;
	}

	public void setListaKategorii(ArrayList<Kategoria> listaKategorii) {
		this.listaKategorii = listaKategorii;
	}
}
