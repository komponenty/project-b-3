package jedenz10.serwer.bazapytan;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import jedenz10.serwer.PlikBazy;
import jedenz10.serwer.Pytanie;
import jedenz10.serwer.interfejsy.IBazaPytanAdmin;

public class CBazaPytan implements IBazaPytanAdmin {
	
	private BazaKategorii baza;
	private PlikBazy plik;
	
	public CBazaPytan(BazaKategorii baza, PlikBazy plik) {
		this.baza = baza;
		this.plik = plik;
		wczytajBaze();
	}
	
	@Override
	public void wczytajBaze() {
		//System.out.println("lol");
		try
		{
			//System.out.println("a");
			//System.out.println(plik);
			String kategoria="";
			File p = new File(plik.toString());
			Scanner zawartoscPliku = new Scanner(p);
			while(zawartoscPliku.hasNextLine()) {
				String kolejnaLiniaTekstu = zawartoscPliku.nextLine();
				//System.out.println(kolejnaLiniaTekstu);
				if(kolejnaLiniaTekstu.matches("^﻿Pierwszy etap;;;;;$")) {
					kategoria="latwe";
					baza.getListaKategorii().add(new Kategoria(kategoria));
					//System.out.println(kategoria);
				} else if(kolejnaLiniaTekstu.matches("^Drugi etap;;;;;$")) {
					kategoria="srednie";
					baza.getListaKategorii().add(new Kategoria(kategoria));
					//System.out.println(kategoria);
				} else if(kolejnaLiniaTekstu.matches("^Trzeci etap;;;;;$")) {
					kategoria="trudne";
					baza.getListaKategorii().add(new Kategoria(kategoria));
					//System.out.println(kategoria);
				}
				if(kolejnaLiniaTekstu.matches("^[1-9][0-9]{0,1};.*$")) {
					String[] pytanie = kolejnaLiniaTekstu.split(";");
					String[] odpowiedzi = new String[4];
					odpowiedzi[0] = pytanie[2];
					odpowiedzi[1] = pytanie[3];
					odpowiedzi[2] = pytanie[4];
					odpowiedzi[3] = pytanie[5];
					baza.dodajPytanie(new Pytanie(pytanie[1],kategoria,odpowiedzi));
					//System.out.println("Pytanie: "+pytanie[1]+"Kategoria: "+kategoria+"Odpowiedzi:"+odpowiedzi);
				}
			}
			zawartoscPliku.close();
		}
		catch(FileNotFoundException e)
		{
			System.out.println("Blad dostepu do pliku!");
			e.printStackTrace();
			System.exit(0);
		}
	}

	@Override
	public void zapiszBaze() {
		// TODO Auto-generated method stub
		
	}
}
