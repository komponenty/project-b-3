/**
 * 
 */
package jedenz10.serwer;

import jedenz10.serwer.interfejsy.IPlikBazy;

public class PlikRankingu implements IPlikBazy {
	
	private String sciezkaDoPliku;
	
	public PlikRankingu(String sciezkaDoPliku) {
		this.sciezkaDoPliku = sciezkaDoPliku;
	}
	
	@Override
	public String sciezkaDoPliku() {
		return sciezkaDoPliku;
	}
	
	@Override
	public String toString() {
		return sciezkaDoPliku();
	}
	
}
