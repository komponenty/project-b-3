/**
 * 
 */
package jedenz10.serwer;

import jedenz10.serwer.interfejsy.IPlikRankingu;

public class PlikBazy implements IPlikRankingu {
	
	private String sciezkaDoPliku;
	
	public PlikBazy(String sciezkaDoPliku) {
		this.sciezkaDoPliku = sciezkaDoPliku;
	}
	
	@Override
	public String sciezkaDoPliku() {
		return sciezkaDoPliku;
	}
	
	@Override
	public String toString() {
		return sciezkaDoPliku();
	}
	
}
