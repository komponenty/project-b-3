package jedenz10.serwer;

import java.io.Serializable;
import java.util.Random;

import jedenz10.serwer.interfejsy.IPytanie;

public class Pytanie implements IPytanie,Serializable {
	
	private String pytanie;
	private String kategoria;
	private String odpowiedzi[];
	
	private static final long serialVersionUID = 1L;
	
	public Pytanie(String pytanie, String kategoria, String odpowiedzi[]) {
		this.pytanie = pytanie;
		this.kategoria = kategoria;
		this.odpowiedzi = odpowiedzi;
	}
	
	@Override
	public String kategoria() {
		// TODO Auto-generated method stub
		return kategoria;
	}

	@Override
	public String[] odpowiedzi() {
		// TODO Auto-generated method stub
		return odpowiedzi;
	}

	@Override
	public boolean czyPrawidlowa(String odpowiedz) {
		if(odpowiedzi[0].equals(odpowiedz)) {
			return true;
		}
		return false;
	}

	@Override
	public String[] podPodpowiedzi() {
		String[] podpowiedzi = new String[4];
		Random r = new Random();
		int l = r.nextInt(4);
		podpowiedzi[l] = odpowiedzi[0];
		switch(l) {
		case 0:{
			podpowiedzi[1] = odpowiedzi[1];
			podpowiedzi[2] = odpowiedzi[2];
			podpowiedzi[3] = odpowiedzi[3];
		};
		break;
		case 1:{
			podpowiedzi[0] = odpowiedzi[1];
			podpowiedzi[2] = odpowiedzi[2];
			podpowiedzi[3] = odpowiedzi[3];
		};
		break;
		case 2:{
			podpowiedzi[0] = odpowiedzi[1];
			podpowiedzi[1] = odpowiedzi[2];
			podpowiedzi[3] = odpowiedzi[3];
		};
		break;
		case 3:{
			podpowiedzi[0] = odpowiedzi[1];
			podpowiedzi[1] = odpowiedzi[2];
			podpowiedzi[2] = odpowiedzi[3];
		};
		break;
		}
		return podpowiedzi;
	}

	public String getPytanie() {
		return pytanie;
	}

	public void setPytanie(String pytanie) {
		this.pytanie = pytanie;
	}

}
