/**
 * 
 */
package jedenz10.serwer.interfejsy;

import java.util.ArrayList;

import jedenz10.serwer.Gracz;


public interface IRanking {
	ArrayList<Gracz> ranking();
	void dodajPunkty(String nick, int punkty);
	void resetPunktow();
}
