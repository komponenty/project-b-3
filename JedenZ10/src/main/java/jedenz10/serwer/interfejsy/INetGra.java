/**
 * 
 */
package jedenz10.serwer.interfejsy;

import jedenz10.serwer.GraczNet;


public interface INetGra {
	void DodajGraczaDoGry(GraczNet gracz);
	void zadajPytanie(GraczNet gracz);
	void zacznijGre(String nick);
	void dodajPunkty(String gracz,int ilosc);
}
