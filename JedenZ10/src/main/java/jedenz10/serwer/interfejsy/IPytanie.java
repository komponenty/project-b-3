/**
 * 
 */
package jedenz10.serwer.interfejsy;

import java.io.Serializable;


public interface IPytanie extends Serializable {
	String kategoria();
	String[] odpowiedzi();
	boolean czyPrawidlowa(String odpowiedz);
	String[] podPodpowiedzi();
}
