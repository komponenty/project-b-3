package jedenz10.serwer;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainServer {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = 
	             new ClassPathXmlApplicationContext("Beans.xml");
		
		/*ListaRankingowa listaRankingowa = (ListaRankingowa) context.getBean("listaRankingowa");
		//listaRankingowa.dodajPunkty("bca", 3);
		
		System.out.println(listaRankingowa.ranking().size());
		Object[] a = listaRankingowa.ranking().toArray();
		Arrays.sort(a);
		String pakietRanking = a[0].toString();
		for(int i = 1; i < a.length; i++) {
			pakietRanking += "\n"+a[i].toString();
		}
		System.out.println(pakietRanking);
		
		SterownikGry sterownikGry = (SterownikGry) context.getBean("sterownikGry");
		sterownikGry.ranking();
		//listaRankingowa.resetPunktow();*/
		
		Server server = (Server) context.getBean("server");
		server.start();
		
		context.close();
	}
	
}
