package jedenz10.serwer.ranking;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import jedenz10.serwer.Gracz;
import jedenz10.serwer.PlikRankingu;
import jedenz10.serwer.interfejsy.IRanking;

public class ListaRankingowa implements IRanking {
	
	private ArrayList<Gracz> gracze;
	private PlikRankingu plik;
	
	@Override
	public ArrayList<Gracz> ranking() {
		try
		{
			gracze = new ArrayList<Gracz>();
			File plik = new File(this.plik.toString());
			Scanner zawartoscPliku = new Scanner(plik);
			while(zawartoscPliku.hasNextLine()) {
				String kolejnaLiniaTekstu = zawartoscPliku.nextLine();
				String[] ranking = kolejnaLiniaTekstu.split(";");
				gracze.add(new Gracz(ranking[0],Integer.valueOf(ranking[1])));
			}
			zawartoscPliku.close();
		}
		catch(FileNotFoundException e)
		{
			System.out.println("Blad dostepu do pliku!");
			e.printStackTrace();
			System.exit(0);
		}
		return gracze;
	}

	@Override
	public void dodajPunkty(String nick, int punkty) {
		gracze = new ArrayList<Gracz>();
		gracze.add(new Gracz(nick,punkty));
		try {
		    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(plik.toString(), true)));
		    for(int i=0; i < gracze.size(); i++) {
		    	out.println(gracze.get(i).nickaGracza()+";"+gracze.get(i).liczbaPunktow());
		    }
		    out.close();
		} catch (IOException e) {
		    System.out.println("Blad Podczas zapisu pliku");
		}
	}
	
	@Override
	public void resetPunktow() {
		try {
		    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(plik.toString(), false)));
		    out.print("");
		    out.close();
		} catch (IOException e) {
		    System.out.println("Blad Podczas zapisu pliku");
		}
	}
	
	public PlikRankingu getPlik() {
		return plik;
	}

	public void setPlik(PlikRankingu plik) {
		this.plik = plik;
	}
}
